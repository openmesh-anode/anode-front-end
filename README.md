# aNode Front-end Overview

aNode is a solution to the OpenD/I Hackathon problem 3. This is the repository for the front-end of the aNode project.
aNode means "a node": every node is the same, which indicates that this system is completely decentralized and P2P.



## Project Overview

### System Understanding

- **Goal**: To create a decentralized data cloud system that mirrors key features of Xnode, enabling rapid development and deployment, emphasizing ease of use, affordability, and scalability.
- **Key Features**:
  - Decentralized Data Connectivity
  - Rapid Deployment and Scalability
  - Cost-Effective Infrastructure

### User Interface Design

- **Dynamic Dashboard**: Displays real-time data and system status, visualized through charts and metrics.
- **System Configuration Interface**: Allows users to configure and manage data sources, APIs, and other system parameters.
- **Real-Time Data Flow Display**: Shows data streams received from WebSocket and REST APIs.

## Technical Stack

- **Frontend Framework**: React
- **UI Library**: Ant Design Pro
- **State Management**: Redux / Context API
- **Real-Time Communication**: WebSocket / socket.io
- **Data Visualization**: D3.js
- **Containerization**: Docker

## Development Plan and Implementation

### Frontend Architecture

- **Modularization**: Division into multiple modules like data display, configuration management, and API testing.
- **Component Design**: Creation of reusable components like data cards, charts, forms, etc.

### Data Flow and State Management

- Management of data streams from WebSocket and REST APIs.
- State management using Redux or Context API for connection status, data sources, etc.

### Real-Time Data Processing

- WebSocket client to receive and display real-time data on the dashboard.
- Data caching and on-demand update strategies for handling large data streams.

### Security and Performance

- Secure data transmission using WebSocket Secure (wss).
- Frontend application performance optimization, including code splitting and lazy loading.

### Responsive Design

- Creation of a responsive interface adaptable to various devices and screen sizes.

### Internationalization

- Multi-language support to cater to a diverse user base.

## Testing and Deployment

- **Unit and Integration Testing**: To ensure component and feature reliability.
- **Containerized Deployment**: Using Docker to encapsulate the frontend application, simplifying the deployment process.

## Documentation and Maintenance

- Detailed development documentation and user manuals will be provided.
- Code structure designed for easy maintenance and scalability.

## Technical Challenges and Solutions

### Handling Large Volume of Real-Time Data

- Use WebWorkers or server-side push to offload tasks from the main thread.
- Implement data caching and intelligent update mechanisms.

### WebSocket Connection Management

- Automatic reconnection and heartbeat mechanisms to maintain stable connections.
- Efficient management of WebSocket connections to prevent resource leaks.

### Dynamic Configuration and Scalability

- Flexible configuration interfaces allowing users to easily add or modify data sources.
- Architectural design considering future functionality and expandability.

## Getting Started

### Prerequisites

- Node.js (version 12 or higher)
- npm or yarn
- Docker (for containerized deployment)

### Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/cloud-captains-of-unsw/anode-front-end.git
   ```

2. avigate to the project directory:

   ```
   cd anode-front-end
   ```

3. Install dependencies:

   ```
   npm install
   ```

   or

   ```
   yarn install
   ```

4. Start the development server:

   ```
   npm start
   ```

   or

   ```
   yarn start
   ```

### Docker Deployment

To deploy using Docker, run:

```
bashCopy code
docker build -t anode-front-end .
docker run -p 80:80 anode-front-end
```

## Usage

The frontend can be accessed at `http://localhost` after starting the development server or deploying using Docker.

## Contributing

Contributions are welcome! Please read our [Contributing Guide](http://localhost) for more information.

## License

This project is licensed under the [MIT License](http://localhost).

## Contact

For any queries or contributions, please contact [Hailey] at [mahuirong01@gmail.com](mailto:email@example.com).

 
